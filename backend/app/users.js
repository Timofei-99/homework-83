const express = require('express');
const User = require('../models/User');
const bcrypt = require('bcrypt');

const router = express.Router();


router.post('/', async (req, res) => {
    const user = req.body;
    if (!user.username || !user.password) {
        res.status(400).send({error: 'Data not valid'});
    }

    const userData = {
        username: user.username,
        password: user.password
    };

    const userBody = new User(userData);

    try {
        userBody.generateToken();
        await userBody.save();
        res.send(userBody);
    } catch (e) {
        res.sendStatus(500);
    }

});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(401).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({error: 'Password is wrong'});
    }

    user.generateToken();
    await user.save();

    res.send({message: 'Username and password correct!', user});
});


module.exports = router;