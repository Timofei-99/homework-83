const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const users = require('./app/users');
const track_history = require('./app/trackHistory');


const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/artists', artists);
app.use('/albums', albums);
app.use('/tracks', tracks);
app.use('/users', users);
app.use('/track_history', track_history);

const port = 8000;


const run = async () => {
    await mongoose.connect('mongodb://localhost/fm');

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
};

run().catch(e => console.log(e));